package getMaximum;

import ui.UI;

public class MaximumUI extends UI {

    public static void main(String[] args) {
        Maximum maximum = new Maximum();

        int int1 = getIntInput();
        int int2 = getIntInput();
        int int3 = getIntInput();

        int result = maximum.getMaximum(int1,int2,int3);

        out("Das Maximum ist " + result);


    }
}
