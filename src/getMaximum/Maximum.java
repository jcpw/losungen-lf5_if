package getMaximum;

public class Maximum {
    public int getMaximum(int number1, int number2, int number3) {

        int highest = number1;

        if(number2 > highest) {
            highest = number2;
        }

        if(number3 > highest) {
            highest = number3;
        }
        return highest;
    }
}

