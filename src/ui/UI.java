package ui;

import java.util.Scanner;

abstract public class UI {

    protected static int getIntInput() {

        return getIntInput("Gib eine Zahl ein: ");

    }

    protected static int getIntInput(String askingText) {

        System.out.print(askingText);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextInt();
    }

    protected static double getDoubleInput(String askingText) {

        System.out.print(askingText);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextDouble();
    }

    protected static char getCharInput(String askingText) {

        System.out.print(askingText);
        Scanner scanner = new Scanner(System.in);
        return scanner.nextLine().charAt(0);
    }

    protected static void out(String message) {
        System.out.println(message);
    }

}
