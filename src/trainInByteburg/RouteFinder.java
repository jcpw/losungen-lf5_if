package trainInByteburg;

public class RouteFinder {

    private final int RINGLINETRANSITIONSTATION = 3;

    public TicketRoute findBetween(Stop start, Stop end) throws Exception {

        TicketRoute ticket1 = findBetweenNotUsingRingLine(start, end);

        if (areStopsOnSameLine(start, end) && start.getStop() != RINGLINETRANSITIONSTATION) {
            return ticket1;
        }

        TicketRoute ticket2 = findBetweenUsingRingLine(start, end);

        return ticket1.getLength() > ticket2.getLength() ? ticket2 : ticket1;
    }

    private TicketRoute findBetweenUsingRingLine(Stop start, Stop end) throws Exception {

        Stop transitionStop1 = Stops.create(start.getLine(), RINGLINETRANSITIONSTATION);
        Stop transitionStop2 = Stops.create(end.getLine(), RINGLINETRANSITIONSTATION);

        TicketRoute route = new TicketRoute();

        if (!start.equals(transitionStop1)) {
            TicketRoute route1 = findBetweenStopsOnSameLine(start, transitionStop1);
            route.appendRoute(route1);
        }

        if (!transitionStop1.equals(transitionStop2)) {
            TicketRoute ringroute = findBetweenStopsOnRingLine(transitionStop1, transitionStop2);
            route.appendRoute(ringroute);
        }

        if (!end.equals(transitionStop2)) {
            TicketRoute route2 = findBetweenStopsOnSameLine(transitionStop2, end);
            route.appendRoute(route2);
        }

        return route;
    }

    private TicketRoute findBetweenStopsOnRingLine(Stop ringlineStart, Stop ringlineEnd) throws Exception {

        if (ringlineStart.getStop() != RINGLINETRANSITIONSTATION || ringlineEnd.getStop() != RINGLINETRANSITIONSTATION) {
            throw new RuntimeException("Diese Halte liegen nicht auf der Ringlinie"); //todo refactor: use own excep
        }

        TicketRoute route = new TicketRoute();

        int currentLine = Math.min(ringlineStart.getLine(), ringlineEnd.getLine());
        int higherLine = Math.max(ringlineStart.getLine(), ringlineEnd.getLine());

        if (shouldGoClockwise(currentLine, higherLine)) {

            while (currentLine <= higherLine) {
                route.addStop(Stops.create(currentLine, RINGLINETRANSITIONSTATION));
                currentLine++;
            }

        } else {

            while (currentLine != higherLine) {
                route.addStop(Stops.create(currentLine, RINGLINETRANSITIONSTATION));
                currentLine--;
                if (currentLine == 0) {
                    currentLine = Stops.LINESCOUNT;
                }
            }
            route.addStop(Stops.create(higherLine, RINGLINETRANSITIONSTATION));
        }

        return route;

    }

    private boolean shouldGoClockwise(int line, int line2) {

        int higherstop = Math.max(line, line2);
        int lowerstop = Math.min(line, line2);
        return (lowerstop + Stops.LINESCOUNT - higherstop) > (higherstop - lowerstop);

    }

    private TicketRoute findBetweenNotUsingRingLine(Stop start, Stop end) throws Exception {

        if (areStopsOnSameLine(start, end)) {

            return findBetweenStopsOnSameLine(start, end);

        } else {

            return findBetweenStopsGoingOver00(start, end);
        }
    }

    private TicketRoute findBetweenStopsGoingOver00(Stop start, Stop end) throws Exception {

        TicketRoute route1 = findBetweenStopsOnSameLine(start, Stops.create(start.getLine(), 0));
        TicketRoute route2 = findBetweenStopsOnSameLine(Stops.create(end.getLine(), 0), end);

        return route1.appendRoute(route2);

    }

    private TicketRoute findBetweenStopsOnSameLine(Stop start, Stop end) throws Exception {
        TicketRoute route = new TicketRoute();

        Stop mainstation = Stops.create(0, 0);

        if (start.equals(mainstation)) {
            start = Stops.create(end.getLine(), 0);
        }

        if (end.equals(mainstation)) {
            end = Stops.create(start.getLine(), 0);
        }

        int line = start.getLine();
        int currentStop = start.getStop();

        if (start.getStop() > end.getStop()) {

            while (currentStop >= end.getStop()) {
                route.addStop(Stops.create(line, currentStop));
                currentStop--;
            }

        } else {

            while (currentStop <= end.getStop()) {
                route.addStop(Stops.create(line, currentStop));
                currentStop++;
            }

        }

        return route;

    }

    private boolean areStopsOnSameLine(Stop start, Stop end) {

        return start.getLine() == end.getLine() || (start.getLine() == 0 || end.getLine() == 0);

    }
}
