package trainInByteburg;

public class Stops {

    static final int LINESCOUNT = 5;
    static final int MAX_STOPS = 6;


    static Stop create(int combinedInt) throws Exception {
        int stop = combinedInt % 10;
        int line = (combinedInt - stop) / 10;

        return create(line, stop);

    }

    static Stop create(int line, int stop) throws Exception {
        if (line > LINESCOUNT || line < 0) {
            throw new Exception("Nicht existierende Bahnlinie " + line);
        }
        if (stop > MAX_STOPS || stop < 0) {
            throw new Exception("Nicht existierende Haltestelle " + stop);
        }
        return new Stop(line * 10 + stop);
    }
}
