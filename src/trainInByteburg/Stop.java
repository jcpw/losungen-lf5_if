package trainInByteburg;

public class Stop {

    private final int line;
    private final int stop;

    public Stop(int combinedInt) { // todo: remove / deprecate
        stop = combinedInt % 10;
        line = (combinedInt - stop) / 10;

    }

    public int getStop() {
        return stop;
    }

    public int getLine() {
        return line;
    }

    public boolean equals(Stop stop) {

        return getStop() == stop.getStop() && getLine() == stop.getLine();

    }

}
