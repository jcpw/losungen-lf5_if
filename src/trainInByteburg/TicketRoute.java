package trainInByteburg;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TicketRoute {

    private static final List FINALDESTINATIONS = Arrays.asList(6);
    private final ArrayList<Stop> stops = new ArrayList<>();
    private int crossedZones = 0;

    public void addStop(Stop stop) throws Exception {

        evaluateIfZonesAreCrossed(stop);

        if (stop.getStop() == 0) {
            stop = Stops.create(0, 0);
        }

        stops.add(stop);
    }

    public TicketRoute appendRoute(TicketRoute route) {

        if (stops.size() > 0 && stops.get(stops.size() - 1).equals(route.stops.get(0))) {
            route.stops.remove(0);
        }

        stops.addAll(route.stops);
        crossedZones += route.crossedZones;


        return this;
    }

    public int getLength() {
        return stops.size();
    }

    public int getCrossedZones() {
        return crossedZones;
    }

    public ArrayList<Stop> getFinalStations() {

        ArrayList<Stop> results = new ArrayList<>();

        for (Stop stop : stops) {
            if (FINALDESTINATIONS.contains(stop.getStop())) {
                results.add(stop);
            }
        }

        return results;
    }

    private void evaluateIfZonesAreCrossed(Stop stop) {
        Stop previousStop = getLastAddedStop();

        if (previousStop != null &&
                (
                    previousStop.getStop() == 3 && stop.getStop() == 4 ||
                    previousStop.getStop() == 4 && stop.getStop() == 3
                )
        ) {
            crossedZones++;
        }
    }

    private Stop getLastAddedStop() {
        if (stops.size() == 0) {
            return null;
        }

        return stops.get(stops.size() - 1);
    }
}
