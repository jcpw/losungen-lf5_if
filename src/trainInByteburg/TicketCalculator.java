package trainInByteburg;

public class TicketCalculator {

    private final int MINIMUMFARE = 2;

    public int calculateTicketprice(int startRailwayStation, int exitRailwayStation) {

        RouteFinder finder = new RouteFinder();

        try {

            Stop start = Stops.create(startRailwayStation);
            Stop end = Stops.create(exitRailwayStation);

            TicketRoute route = finder.findBetween(start, end);

            return getPriceFromRoute(route);

        } catch (Exception exception) {
            return -1;
        }
    }

    private int getPriceFromRoute(TicketRoute route) {

        int price = MINIMUMFARE;

        if(route.getLength() == 2) {
            return 1;
        }

        price += route.getCrossedZones();

        price += countFinalStations(route);

        return price;
    }

    private int countFinalStations(TicketRoute route) {

        return route.getFinalStations().size();
    }
}