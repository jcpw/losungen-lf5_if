package trainInByteburg;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class RouteFinderTest {

    @Test
    public void findBestRoute() throws Exception {
        RouteFinder finder = new RouteFinder();
        Stop stop1 = Stops.create(1,2);
        Stop stop2 = Stops.create(3, 5);
        TicketRoute route = finder.findBetween(stop1, stop2);

        assertEquals(6, route.getLength());
    }

}