package calculator;

import ui.UI;

public class CalculatorUI extends UI {

    public static void main (String[] args) {
        double num1 = getDoubleInput("Gib die erste Zahl ein: ");
        double num2 = getDoubleInput("Gib die erste Zahl ein: ");
        char operator = getCharInput("Gib das Rechenzeichen ein: ");

        Calculator calc = new Calculator();

        out(calc.calculate(num1, num2, operator));
    }
}
