package calculator;

public class Calculator {

    public String calculate(double number1, double number2, char operator) {
        double result = 0;

        switch (operator) {
            case '+' -> {
                result = add(number1, number2);
            }
            case '/' -> {
                try {
                    result = divide(number1, number2);
                    operator = ':';
                } catch (DivisionByZeroException excep) {
                    return excep.getMessage();
                }
            }
            case '*' -> {
                result = multiply(number1, number2);
                operator = 'x';
            }
            case '-' -> {
                result = substract(number1, number2);
            }

            default -> {
                try {
                    throw new UnknownOperatorException("Sie müssen einen der vier Operatoren +,-,*,/ eingeben!");
                } catch (UnknownOperatorException excep) {
                    return excep.getMessage();
                }
            }
        }

        return number1 + " " + operator + " " + number2 + " = " + result;
    }

    private double substract(double number1, double number2) {
        return number1 - number2;
    }

    private double multiply(double number1, double number2) {

        return number1 * number2;
    }

    private double divide(double number1, double number2) throws DivisionByZeroException {
        if(number2 == 0) {
            throw new DivisionByZeroException("Division durch 0 nicht möglich!");
        }

        return number1 / number2;
    }

    private double add(double number1, double number2) {
        return number1 + number2;
    }
}

