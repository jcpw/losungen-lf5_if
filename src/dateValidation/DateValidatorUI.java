package dateValidation;

import ui.UI;

public class DateValidatorUI extends UI {

    public static void main (String[] args) {

        int day = getIntInput("Gib den Tag ein: ");
        int month = getIntInput("Gib den Monat ein: ");
        int year = getIntInput("Gib das Jahr ein: ");

        DateValidator datevali = new DateValidator();
        boolean result = datevali.validateDate(day, month, year);

        out("Das ist ein " + (result ? "" : "un") + "gültiges Datum.");
    }
}
