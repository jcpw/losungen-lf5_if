package dateValidation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DateValidator {

    public boolean validateDate(int day, int month, int year) {

        boolean isSchaltjahr = isSchaltjahr(year);

        boolean isValidMonth = isValidMonth(month);

        boolean isValidDay = isValidDay(day, month, isSchaltjahr);

        return isValidMonth && isValidDay;
    }

    private boolean isValidDay(int day, int month, boolean isSchaltjahr) {

        if(day < 1) return false;

        if(month == 2) {
            return day < (isSchaltjahr ? 30 : 29);
        }

        List monthsWith30Days = Arrays.asList(4,6,9,11);
        List monthsWith31Days = Arrays.asList(1,3,5,7,8,10,12);

        if(monthsWith30Days.contains(month)) {
            return day < 31;
        }

        if(monthsWith31Days.contains(month)) {
            return day < 32;
        }

        return false;
    }


    private boolean isValidMonth(int month) {

        return month > 0 && month < 13;
    }

    private boolean isSchaltjahr(int year) {
        return year % 400 == 0 ||
                (year % 4 == 0 && year % 100 != 0);
    }



}
