package maexchen;

public class Maexchen {

    public int calculatePoints(int throw1, int throw2){

        if(!isValidThrow(throw1) || !isValidThrow(throw2)) {
            return -1;
        }

        if(throw1 == throw2) {
            return throw1 * 100;
        }

        if(throw1 + throw2 == 3) {
            return 1000;
        }

        if(throw1 > throw2) {
            return throw1*10 + throw2;
        } else {
            return throw2*10 + throw1;
        }

    }

    private boolean isValidThrow(int dicethrow) {
        return dicethrow > 0 && dicethrow < 7;
    }
}
