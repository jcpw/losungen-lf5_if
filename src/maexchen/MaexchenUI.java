package maexchen;

import ui.UI;

import java.util.Random;

public class MaexchenUI extends UI {

    public static void main(String[] args) {
        int throw1 = 0;
        int throw2 = 0;
        int mode = getIntInput("Wie willst du spielen? 0 = zufällig, 1 = manuell ");
        if (mode == 0) {

            throw1 = getRandomDiceThrow();
            throw2 = getRandomDiceThrow();
        } else if (mode == 1) {

            throw1 = getIntInput("Was hast du gewürfelt? ");
            throw2 = getIntInput("Was hast du gewürfelt? ");
        }

        Maexchen maexchen = new Maexchen();

        int points = maexchen.calculatePoints(throw1, throw2);

        out("Du erhälst " + points + " Punkte");
    }

    private static int getRandomDiceThrow() {

        Random generator = new Random();
        int dicethrow = generator.nextInt(5) + 1;
        out("Du hast eine " + dicethrow + " gewürfelt");
        return dicethrow;
    }
}
